namespace npoCma.Models.Enums
{
    public enum Departments
    {
        Labs = 1,
        ProcessControlDesign = 2,
        Accounting = 3,
        CommissioningStartUp = 4,
        Directors = 5
    }
}
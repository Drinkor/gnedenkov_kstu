using System;
using System.ComponentModel.DataAnnotations;

namespace npoCma.Models.databaseModels
{
    public class VacationHistory
    {
        public int UserId { get; set; }
        public DateTime StartVacation { get; set; }
        public DateTime EndVacation { get; set; }
        public double VacationPay { get; set; }
        [Key] public Guid DocumentId { get; set; }
        public bool IsAnnual { get; set; }
    }
}
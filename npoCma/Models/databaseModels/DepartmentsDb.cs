using System.ComponentModel.DataAnnotations;

namespace npoCma.Models.databaseModels
{
    public class DepartmentsDb
    {
        [Key] public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public int HeadOfDepartment { get; set; }
    }
}
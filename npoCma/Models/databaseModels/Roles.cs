using System.Collections.Generic;

namespace npoCma.Models.databaseModels
{
    public class Role
    {
        public Role()
        {
            Users = new List<User>();
        }

        public int RoleId { get; set; }
        public string RoleName { get; set; }
        private List<User> Users { get; }
    }
}
using Microsoft.EntityFrameworkCore;
using npoCma.Models.databaseModels;

namespace npoCma.Models
{
    public sealed class UserContext : DbContext
    {
        public UserContext(DbContextOptions<UserContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        public DbSet<User> Users { get; set; }
        public DbSet<VacationHistory> VacationHistory { get; set; }
        public DbSet<DepartmentsDb> Departments { get; set; }
    }
}
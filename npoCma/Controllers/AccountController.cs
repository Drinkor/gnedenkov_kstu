using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using npoCma.Models;
using npoCma.Models.databaseModels;
using npoCma.Models.Enums;
using npoCma.ViewModels;

namespace npoCma.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserContext _db;

        public AccountController(UserContext context)
        {
            _db = context;
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _db.Users.FirstOrDefaultAsync(u =>
                    u.Email == model.Email && u.Password == model.Password);
                if (user != null)
                {
                    await Authenticate(model.Email);

                    return RedirectToAction("Cabinet");
                }

                ModelState.AddModelError("", "Некорректные логин и(или) пароль");
            }

            return View(model);
        }

        [HttpGet]
        [Authorize]
        public IActionResult Cabinet()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Cabinet(CabinetModel cabinetModel)
        {
            if (ModelState.IsValid)
            {
                var user = _db.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name);

                var headOfUser = _db.Departments.FirstOrDefault(u => u.DepartmentId == user.Result.Department)
                    ?.HeadOfDepartment;

                if ((user.Result.UserId == headOfUser) & (user.Result.Department == (int) Departments.Accounting))
                    headOfUser = 18;

                if ((user.Result.UserId == headOfUser) & (user.Result.Department != (int) Departments.Accounting))
                    headOfUser = 19;

                var endVacation = cabinetModel.EndVacation.AddHours(23).AddMinutes(59).AddSeconds(59);

                var emailHeadOfDepartment = _db.Users.FirstOrDefault(u => u.UserId == headOfUser)?.Email;

                var dayOfVacations = (endVacation - cabinetModel.StartVacation).Days;

                var salary = _db.Users.FirstOrDefaultAsync(u => u.Email == User.Identity.Name).Result.Salary;

                var vacationPay = 0.00;

                if (cabinetModel.IsAnnual) vacationPay = Math.Round(salary * 12 / 12 / 29.3 * dayOfVacations, 2);

                await _db.VacationHistory.AddAsync(new VacationHistory
                {
                    StartVacation = cabinetModel.StartVacation, EndVacation = endVacation,
                    DocumentId = Guid.NewGuid(), VacationPay = vacationPay, UserId = user.Result.UserId,
                    IsAnnual = cabinetModel.IsAnnual
                });
                await _db.SaveChangesAsync();

                await EmailSending.SendEmailAsync(cabinetModel.IsAnnual, emailHeadOfDepartment, user.Result.LastName,
                    user.Result.FirstName, user.Result.MiddleName, cabinetModel.StartVacation,
                    cabinetModel.EndVacation);
            }

            return View(cabinetModel);
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _db.Users.FirstOrDefaultAsync(u => u.Email == model.Email);
                if (user == null)
                {
                    await _db.Users.AddAsync(new User {Email = model.Email, Password = model.Password});
                    await _db.SaveChangesAsync();

                    await Authenticate(model.Email);

                    return RedirectToAction("Index", "Home");
                }

                ModelState.AddModelError("", "Некорректные логин и(или) пароль");
            }

            return View(model);
        }

        private async Task Authenticate(string userName)
        {
            // создаем один claim
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, userName)
            };
            // создаем объект ClaimsIdentity
            var id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);
            // установка аутентификационных куки
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Login", "Account");
        }
    }
}
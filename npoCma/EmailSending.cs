using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace npoCma
{
    public static class EmailSending
    {
        public static async Task SendEmailAsync(bool isAnnual, string email, string lastName, string firstName,
            string middleName, DateTime startVacation, DateTime endVacation)
        {
            var vacationReason = isAnnual ? "ежегодный оплачиваемый" : "за свой счет";

            var from = new MailAddress("no-reply@npocma.com", "Уведомление");
            var to = new MailAddress(email);
            var m = new MailMessage(from, to)
            {
                Subject = "Заявление на отпуск",
                Body =
                    $"Сотрудник {lastName} {firstName} {middleName} хочет оформить отпуск {vacationReason} с  {startVacation} по {endVacation}"
            };
            var smtp = new SmtpClient("smtp.npocma.com", 587)
            {
                Credentials = new NetworkCredential("no-reply@npocma.com", "mypassword"), EnableSsl = true
            };
            await smtp.SendMailAsync(m);
        }
    }
}
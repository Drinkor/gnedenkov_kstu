using System;
using System.ComponentModel.DataAnnotations;

namespace npoCma.ViewModels
{
    public class CabinetModel
    {
        [Required(ErrorMessage = "Не указана дата начала отпсука")]
        public DateTime StartVacation { get; set; }

        [Required(ErrorMessage = "Не указана дата окончания отпуска")]
        public DateTime EndVacation { get; set; }

        public bool IsAnnual { get; set; }
    }
}
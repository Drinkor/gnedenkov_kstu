#docker build -t cardProcessingPrivateApi  -f ePayments.Card.Processing.PrivateApi.dockerfile --rm  .
#docker run --name cardProcessingPrivateApiAInstance   --rm -it -p 8000:80 -e ASPNETCORE_ENVIRONMENT="test" -e 
# -v C:/logs/:/var/logs/cardProcessingPrivateApi/ cardProcessingPrivateApi

FROM mcr.microsoft.com/dotnet/core/sdk:3.1  AS build
WORKDIR /src

COPY /npoCma ./npoCma

WORKDIR /src/npoCma
RUN dotnet publish ./npoCma.csproj -c Release -o /app

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS runtime
WORKDIR /app
EXPOSE 80
COPY --from=build /app .

ENTRYPOINT ["dotnet", "npoCma.dll"]
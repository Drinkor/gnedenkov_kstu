create DATABASE npo_cma
    COLLATE Cyrillic_General_CS_AS;
go
SET ANSI_PADDING ON
go
use npo_cma;
go

CREATE TABLE [dbo].[Users]
(
    [UserId]     [int]           NOT NULL,
    [FirstName]  [nvarchar](255) NOT NULL,
    [LastName]   [nvarchar](255) NOT NULL,
    [MiddleName] [nvarchar](255) not null,
    [Department] [int]           NOT NULL,
    [Email]      varchar(50)     not null,
    [Password]   varchar(50)     not null,
    [RoleId]     [int]           not null,
    [Salary]     [float]         not null
)
go

CREATE TABLE [dbo].[Departments]
(
    [DepartmentId]     [int]           NOT NULL,
    [DepartmentName]   [nvarchar](255) NOT NULL,
    [HeadOfDepartment] [int]           NOT NULL,
)
go

CREATE TABLE [dbo].[VacationHistory]
(
    [UserId]        [int]           NOT NULL,
    [StartVacation] [datetime]      NOT NULL,
    [EndVacation]   [datetime]      NOT NULL,
    [VacationPay]   [float]         not null,
    [DocumentId]    [nvarchar](255) not null,
    [IsAnnual]      [BIT]           not null
)
go

CREATE TABLE [dbo].[Roles]
(
    [RoleId]   [int]           NOT NULL,
    [RoleName] [nvarchar](255) NOT NULL
)
go

insert into [dbo].Roles(RoleId, RoleName)
VALUES (1, 'Сотрудник'),
       (2, 'Руководитель отдела'),
       (3, 'Заместитель'),
       (4, 'Директор')
go

insert into [dbo].Departments(DepartmentId, DepartmentName, HeadOfDepartment)
VALUES (1, 'Лаборатории', 1),
       (2, 'Отдел проектирования АСУТП', 2),
       (3, 'Бухгалтерия', 3),
       (4, 'Пуско - наладочное управление', 4),
       (5, 'Директорат', 19)
go

INSERT INTO dbo.Users (UserId, FirstName, LastName, MiddleName, Department, Email, Password, RoleId, Salary)
VALUES (1, 'Ильдар', 'Сабиров', 'Абузарович', 1, 'isabirov@npocma.com', 'qwerty', 2, 25000),
       (2, 'Александр', 'Самойлов', 'Васильевич', 2, 'asamoylov@npocma.com', 'qwerty', 2, 25000),
       (3, 'Инна', 'Шварева', 'Владимировна', 3, 'ishareva@npocma.com', 'qwerty', 2, 27500),
       (4, 'Нияз', 'Зиганшин', 'Гарифович', 4, 'nziganshin@npocma.com', 'qwerty', 2, 33000),
       (5, 'Ольга', 'Егорова', 'Владимировна', 1, 'oegorova@npocma.com', 'qwerty', 1, 34000),
       (6, 'Денис', 'Березкин', 'Михайлович', 1, 'dberezkin@npocma.com', 'qwerty', 1, 37000),
       (7, 'Александра', 'Викторова', 'Николаевна', 1, 'aviktorova@npocma.com', 'qwerty', 1, 33000),
       (8, 'Даниил', 'Бусыгин', 'Юрьевич', 1, 'dbusygin@npocma.com', 'qwerty', 1, 38000),
       (9, 'Исбулат', 'Домухамедов', 'Фаритович', 2, 'idomuhamedov@npocma.com', 'qwerty', 1, 43000),
       (10, 'Дина', 'Большекова', 'Наилевна', 2, 'dbolshekova@npocma.com', 'qwerty', 1, 23000),
       (11, 'Александр', 'Тимкачев', 'Николаевич', 2, 'atimkachev@npocma.com', 'qwerty', 1, 43000),
       (12, 'Екатерина', 'Каюмова', 'Валерьевна', 2, 'ekaumova@npocma.com', 'qwerty', 1, 17000),
       (13, 'Энза', 'Газизянова', 'Яппаровна', 3, 'egazizynova@npocma.com', 'qwerty', 1, 15000),
       (14, 'Наталья', 'Куприна', 'Адольфовна', 3, 'nkuprina@npocma.com', 'qwerty', 1, 15000),
       (15, 'Марат', 'Садтретдинов', 'Абрарович', 4, 'msadtredinov@npocma.com', 'qwerty', 1, 18000),
       (16, 'Руслан', 'Митрухин', 'Петрович', 4, 'rmitruhin@npocma.com', 'qwerty', 1, 19000),
       (17, 'Александр', 'Кузьмин', 'Васильевич', 4, 'akuzmin@npocma.com', 'qwerty', 1, 21000),
       (18, 'Гнеденкова', 'Оксана', 'Витальевна', 5, 'ognedenkova@npocma.com', 'qwerty', 3, 60000),
       (19, 'Алексей', 'Селехов', 'Владимирович', 5, 'aselikhov@npocma.com', 'qwerty', 3, 60000),
       (20, 'Рефкат', 'Хакимов', 'Мубаракшович', 5, 'rkhakimov@npocma.com', 'qwerty', 4, 70000)
go
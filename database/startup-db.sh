echo 'Starting database setup'

# Убираем стандартный порт чтобы контейнер, который ожидает зависимости, не мог достучатся, но при 
# этом мы сами могли прогнать скрипты для создания таблиц
/opt/mssql/bin/mssql-conf set network.tcpport 5500

# Ждем пока сервер не стартанет
/opt/mssql/bin/sqlservr &
echo "Waiting for MS SQL to be available"
/opt/mssql-tools/bin/sqlcmd -l 30 -S localhost,5500 -U sa -P "$SA_PASSWORD" -Q "SET NOCOUNT ON SELECT \"YAY WE ARE UP\" , @@servername"
is_up=$?
while [ $is_up -ne 0 ] ; 
do 
  echo -e "$(date)" 
  /opt/mssql-tools/bin/sqlcmd -l 30 -S localhost,5500 -U sa -P "$SA_PASSWORD" -Q "SET NOCOUNT ON SELECT \"YAY WE ARE UP\" , @@servername"
  is_up=$?
  sleep 1
done
# Теперь запускаем все sql скрипты, которые пробросили раньше/
echo ls /scripts/
for foo in /scripts/*.sql
do 
    /opt/mssql-tools/bin/sqlcmd -S localhost,5500 -U sa -P "$SA_PASSWORD" -l 30 -e -i "$foo"
done

# Меняем порт на стандартный
/opt/mssql/bin/mssql-conf set network.tcpport 1433
# Теперь нам нужно перезапустить sql сервер. К сожалению, в контейнере недоступен 
# systemctl, поэтому воспользуемся хаком и просто убьем процесс
pkill sqlservr
pkill sqlservr
# Тут нужно подождать пока процесс не завершится. 
while ps -C sqlservr > /dev/null; do sleep 1; done
/opt/mssql/bin/sqlservr &

# So that the container doesn't shut down, sleep this thread
sleep infinity